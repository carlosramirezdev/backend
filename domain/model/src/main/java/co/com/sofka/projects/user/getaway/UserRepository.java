package co.com.sofka.projects.user.getaway;

import co.com.sofka.projects.user.User;
import reactor.core.publisher.Flux;

public interface UserRepository {
    Flux<User> findAllUser();
}
