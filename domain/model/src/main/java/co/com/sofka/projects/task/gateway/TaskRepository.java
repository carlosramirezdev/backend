package co.com.sofka.projects.task.gateway;

import co.com.sofka.projects.task.Task;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

public interface TaskRepository {
    Mono<Task> save(Task task);
    Flux<Task> findAllTask();
    Flux<Task> findAllByIncharge(int inchageID);
    Mono<Task> getId(String id);
    Mono<Void> deleteId(String id);
}
