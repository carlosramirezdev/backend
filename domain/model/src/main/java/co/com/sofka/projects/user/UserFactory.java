package co.com.sofka.projects.user;

import reactor.core.publisher.Mono;

public class UserFactory {
    public static Mono<User> create(
            String id,
            String nickname,
            String email
    ){
        return Mono.just(User.builder()
                .id(id)
                .nickname(nickname)
                .email(email)
                .build()
        );
    }
}
