package co.com.sofka.projects.task;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;

import java.util.Date;
@Data
@Builder(toBuilder = true)
@AllArgsConstructor
public class Task {
    String id;
    String title;
    String description;
    Date dateCreate;
    Date dateEstimatedFinish;
    @Builder.Default
    int inchargeId =0 ;
    @Builder.Default
    boolean isOpen = true;
}
