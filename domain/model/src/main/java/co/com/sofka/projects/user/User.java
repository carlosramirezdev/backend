package co.com.sofka.projects.user;


import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;

@Data
@Builder(toBuilder = true)
@AllArgsConstructor
public class User {
    String id;
    String nickname;
    String email;

}
