package co.com.sofka.projects.task;

import reactor.core.publisher.Mono;

import java.util.Date;

public class TaskFactory {
    public static Mono<Task> create(
            String id,
            String title,
            String description,
            Date dateCreate,
            Date dateEstimatedFinish,
            int inchargeId,
            boolean isOpen
    ){
        return Mono.just(Task.builder()
                .id(id)
                .title(title)
                .description(description)
                .dateCreate(dateCreate)
                .dateEstimatedFinish(dateEstimatedFinish)
                .inchargeId(inchargeId)
                .isOpen(isOpen)
                .build()
        );
    }
}
