package co.com.sofka.projects.common;

import lombok.Data;
import lombok.Builder;
import lombok.AllArgsConstructor;

@Data
@Builder(toBuilder = true)
@AllArgsConstructor
public class Location {

	private final String address;
	private final String city;
	private final String state;
	private final String country;

}