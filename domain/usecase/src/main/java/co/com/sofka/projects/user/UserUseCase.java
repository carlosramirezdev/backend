package co.com.sofka.projects.user;

import co.com.sofka.projects.user.getaway.UserRepository;
import lombok.AllArgsConstructor;
import lombok.Data;
import reactor.core.publisher.Flux;

@Data
@AllArgsConstructor
public class UserUseCase {

    private final UserRepository userRepository;

    public Flux<User> all(){
        return userRepository.findAllUser();
    }
}
