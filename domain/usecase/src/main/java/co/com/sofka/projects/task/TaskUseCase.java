package co.com.sofka.projects.task;

import co.com.sofka.projects.task.gateway.TaskRepository;
import lombok.AllArgsConstructor;
import lombok.Data;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

import java.util.Date;

import static co.com.sofka.projects.common.UniqueIDGenerator.uuid;

@Data
@AllArgsConstructor
public class TaskUseCase {

    private final TaskRepository taskRepository;

    public Mono<Task> create(
            String title,
            String description,
            Date dateCreate,
            Date dateEstimatedFinish,
            int inchargeId,
            boolean isOpen
    ){
      return uuid().flatMap( id -> TaskFactory.create(
              id,
              title,
              description,
              dateCreate,
              dateEstimatedFinish,
              inchargeId,
              isOpen
              )

      ).flatMap(taskRepository::save);
    }

    public Flux<Task> all(){
        return taskRepository.findAllTask();
    }

    public Flux<Task> allByInchage(int inchargeId){
        return taskRepository.findAllByIncharge(inchargeId);
    }

    public Mono<Task> getId(String id){
        return taskRepository.getId(id);
    }

    public Mono<Task> update(
            String id,
            String title,
            String description,
            Date dateCreate,
            Date dateEstimatedDate,
            int inchargeId,
            boolean isOpen
    ){
        return taskRepository.getId(id).flatMap( client-> TaskFactory.create(
                id,
                title,
                description,
                dateCreate,
                dateEstimatedDate,
                inchargeId,
                isOpen
        ).flatMap(taskRepository::save));
    }

    public Mono<Void> delete(String id){
        return taskRepository.deleteId(id);
    }
}
