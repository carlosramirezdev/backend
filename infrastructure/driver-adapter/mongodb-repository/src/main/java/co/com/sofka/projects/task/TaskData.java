package co.com.sofka.projects.task;

import lombok.Data;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

import java.util.Date;

@Data
@Document(collection = "tasks")
public class TaskData {
    @Id
    private String id;
    private String title;
    private String description;
    private Date dateCreate;
    private Date dateEstimatedFinish;
    private int inchargeId;
    private boolean isOpen = true;
}
