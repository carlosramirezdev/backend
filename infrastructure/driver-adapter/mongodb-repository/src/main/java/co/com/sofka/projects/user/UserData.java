package co.com.sofka.projects.user;

import lombok.Data;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

@Data
@Document(collection = "users")
public class UserData {
    @Id
    private String id;
    private String nickname;
    private String email;
}
