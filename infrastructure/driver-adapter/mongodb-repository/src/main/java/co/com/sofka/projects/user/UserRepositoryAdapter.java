package co.com.sofka.projects.user;

import co.com.sofka.projects.user.getaway.UserRepository;
import co.com.sofka.reactive_mongodb_repository_common.AdapterOperations;
import org.reactivecommons.utils.ObjectMapper;
import org.springframework.stereotype.Repository;
import reactor.core.publisher.Flux;

@Repository
public class UserRepositoryAdapter extends AdapterOperations<User, UserData, String, UserDataRepository> implements UserRepository {

    public UserRepositoryAdapter(UserDataRepository repository, ObjectMapper mapper){
        super(repository, mapper, obj->mapper.mapBuilder(obj, User.UserBuilder.class).build());
    }

    @Override
    public Flux<User> findAllUser() {
        return doQueryMany(repository.findAll());
    }
}
