package co.com.sofka.projects.task;

import org.springframework.data.repository.query.ReactiveQueryByExampleExecutor;
import org.springframework.data.repository.reactive.ReactiveCrudRepository;
import org.springframework.stereotype.Repository;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

@Repository
public interface TaskDataRepository extends ReactiveCrudRepository<TaskData, String> , ReactiveQueryByExampleExecutor<TaskData> {
    Mono<Task> findTaskDataById(String id);
    Flux<Task> findTaskDataByinchargeId(int id);
}
