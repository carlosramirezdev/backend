package co.com.sofka.projects.task;

import co.com.sofka.projects.task.gateway.TaskRepository;
import co.com.sofka.reactive_mongodb_repository_common.AdapterOperations;
import org.reactivecommons.utils.ObjectMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

@Repository
public class TaskRepositoryAdapter extends AdapterOperations<Task, TaskData, String, TaskDataRepository> implements TaskRepository {

    @Autowired
    public TaskRepositoryAdapter(TaskDataRepository repository, ObjectMapper mapper){
        super(repository, mapper, obj->mapper.mapBuilder(obj, Task.TaskBuilder.class).build());
    }

    @Override
    public Flux<Task> findAllTask() {
        return doQueryMany(repository.findAll());
    }

    @Override
    public Flux<Task> findAllByIncharge(int inchageID) {
        return repository.findTaskDataByinchargeId(inchageID);
    }

    @Override
    public Mono<Task> getId(String id) {
        return repository.findTaskDataById(id);
    }

    @Override
    public Mono<Void> deleteId(String id) {
        return repository.deleteById(id);
    }
}
