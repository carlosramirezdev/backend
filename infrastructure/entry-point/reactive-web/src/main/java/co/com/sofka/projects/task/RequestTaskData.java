package co.com.sofka.projects.task;

import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.Date;

@Data
@NoArgsConstructor
public class RequestTaskData {
    private String id;
    private String title;
    private String description;
    private Date dateCreated;
    private Date dateEstimatedFinish;
    private int inchargeId = 0;
    private boolean isOpen = true;
}
