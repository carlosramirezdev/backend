package co.com.sofka.projects.user;


import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import reactor.core.publisher.Flux;

@CrossOrigin(value = "*")
@RestController
@RequestMapping("/user")
public class UserController {

    private final UserUseCase userUseCase;

    public UserController (UserUseCase _userUseCase){
        this.userUseCase=_userUseCase;
    }

    @GetMapping("/list")
    public Flux<User> findAllUsers(){
        return userUseCase.all();
    }
}
