package co.com.sofka.projects.task;

import org.springframework.web.bind.annotation.*;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

@CrossOrigin(value = "*")
@RestController
@RequestMapping
public class TaskController {

    private final TaskUseCase taskUseCase;

    public TaskController(TaskUseCase _taskUseCase){
        this.taskUseCase=_taskUseCase;
    }

    @PostMapping("/task/create")
    public Mono<Task> create(@RequestBody RequestTaskData taskData){
        return taskUseCase.create(
                taskData.getTitle(),
                taskData.getDescription(),
                taskData.getDateCreated(),
                taskData.getDateEstimatedFinish(),
                taskData.getInchargeId(),
                taskData.isOpen()
        );
    }

    @GetMapping("/task/list")
    public Flux<Task> findAllTask(){
        return taskUseCase.all();
    }

    @GetMapping("/task/list/incharge/{id}")
    public Flux<Task> allByInchage(@PathVariable int id){
        return taskUseCase.allByInchage(id);
    }

    @GetMapping("/task/find/{id}")
    public Mono<Task> findById(@PathVariable String id){
        return taskUseCase.getId(id);
    }
}
