package co.com.sofka.projects.common;

import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
public class LocationData {

	private String address;
	private String city;
	private String state;
	private String country;

}