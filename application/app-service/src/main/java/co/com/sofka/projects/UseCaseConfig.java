package co.com.sofka.projects;

import co.com.sofka.projects.task.TaskUseCase;
import co.com.sofka.projects.task.gateway.TaskRepository;
import co.com.sofka.projects.user.UserUseCase;
import co.com.sofka.projects.user.getaway.UserRepository;
import lombok.Data;
import org.reactivecommons.utils.ObjectMapper;
import org.reactivecommons.utils.ObjectMapperImp;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Data
@Configuration
public class UseCaseConfig {

	@Bean
	public ObjectMapper objectMapper() {
		return new ObjectMapperImp();
	}

	@Bean
	public TaskUseCase taskUseCase(TaskRepository taskRepository){
		return new TaskUseCase(taskRepository);
	}

	@Bean
	public UserUseCase userUseCase(UserRepository userRepository){
		return new UserUseCase(userRepository);
	}

}